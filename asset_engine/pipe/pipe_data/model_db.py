# ----------------------------------------------------------------------------#
# ------------------------------------------------------------------ HEADER --#

"""
:author:
    Jared Webber


:synopsis:
    model_db.py - Engine +MVC and Database Abstractions

:description:
    Engine and Database Abstract Definitions.
    This module provides a high level set of classes and methods for the core
    components of The Application.

    The structure here closely mimics model.py.

:applications:


:see_also:
   model.py

:license:
    see LICENSE.md

"""


# ----------------------------------------------------------------------------#
# ----------------------------------------------------------------- IMPORTS --#


# ----------------------------------------------------------------------------#
# --------------------------------------------------------------- FUNCTIONS --#


# ----------------------------------------------------------------------------#
# ----------------------------------------------------------------- CLASSES --#


class DatabaseCore(object):
    def __init__(self):
        pass


class DatabaseDocument(object):
    def __init__(self):
        pass


class DatabaseElement(object):
    def __init__(self):
        pass


class DatabaseCollection(object):
    def __init__(self):
        pass


class DatabaseContext(object):
    def __init__(self):
        pass


class DatabaseAsset(object):
    def __init__(self):
        pass


class DatabaseAssembly(object):
    def __init__(self):
        pass


class DatabaseProject(object):
    def __init__(self):
        pass


class DatabaseTask(object):
    def __init__(self):
        pass

class DatabaseTool(object):
    def __init__(self):
        pass



class MakeTheoryTask(object):
    def __init__(self, **kwargs):
        # for key, value in kwargs.items():
        #     setattr(self, key, value)
        self.artist_name = kwargs.setdefault('artist_name', None)
        self.asset_category_id = kwargs.setdefault('asset_category_id', None)
        self.asset_folder = kwargs.setdefault('asset_folder', None)
        self.asset_id = kwargs.setdefault('asset_id', None)
        self.asset_name = kwargs.setdefault('asset_name', None)
        self.asset_parent_id = kwargs.setdefault('asset_parent_id', None)
        self.state_name = kwargs.setdefault('state_name', None)
        self.task_artist_id = kwargs.setdefault('task_artist_id', None)
        self.task_creation_date = kwargs.setdefault('task_creation_date', None)
        self.task_name = kwargs.setdefault('task_name', None)
        self.task_task_id = kwargs.setdefault('task_task_id', None)
        self.version = kwargs.setdefault('version', None)
        self.version_creationDate = kwargs.setdefault('version_creationDate', None)
        self.version_id = kwargs.setdefault('version_id', None)


class MakeTheoryAsset(object):
    def __init__(self, **kwargs):
        # for key, value in kwargs.items():
        #     setattr(self, key, value)
        self.asset_category_id = kwargs.setdefault('asset_category_id', None)
        self.asset_creationDate = kwargs.setdefault('asset_creationDate', None)
        self.asset_parent_id = kwargs.setdefault('asset_parent_id', None)
        self.id = kwargs.setdefault('id', None)
        self.name = kwargs.setdefault('name', None)
        self.parent_id = kwargs.setdefault('parent_id', None)


class MakeTheoryShot(object):
    def __init__(self, **kwargs):
        # for key, value in kwargs.items():
        #     setattr(self, key, value)
        self.asset_category_id = kwargs.setdefault('asset_category_id', None)
        self.asset_creationDate = kwargs.setdefault('asset_creationDate', None)
        self.asset_folder = kwargs.setdefault('asset_folder', None)
        self.asset_parent_id = kwargs.setdefault('asset_parent_id', None)
        self.id = kwargs.setdefault('id', None)
        self.name = kwargs.setdefault('name', None)
        self.parent_id = kwargs.setdefault('parent_id', None)


class MakeTheoryFolder(object):
    def __init__(self, **kwargs):
        self.id = kwargs.setdefault('id', None)
        self.is_file = kwargs.setdefault('is_file', None)
        self.mimeType = kwargs.setdefault('mimeType', None)
        self.modifiedDate = kwargs.setdefault('modifiedDate', None)
        self.parent_id = kwargs.setdefault('parent_id', None)
        self.publisher = kwargs.setdefault('publisher', None)
        self.title = kwargs.setdefault('title', None)
        self.url = kwargs.setdefault('url', None)


class MakeTheoryFile(MakeTheoryFolder):
    def __init__(self, **kwargs):
        super().__init__()
        self.fileExtension = kwargs.setdefault('fileExtension', None)
        self.fileSize = kwargs.setdefault('fileSize', None)

