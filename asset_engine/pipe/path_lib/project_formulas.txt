# Valid 'root' disk types are: config, and work.
# Unused 'root' disk types: code, render, store

# Valid keys for use as variable in any of the formulas are:
#   drive, disk_type, project, sequence, shot, asset, asset_type, episode
#   assembly, discipline, work_area, version, wa_version *NOT USED

# 'drive' refers to the user's home directory or letter drive where the pipeline is located, and the top level directory holding the pipeline (see example below)
# 'drive' = /home/user/3dstuff/theory/
# ---------------------------------------------------------------------------------------------
 
#################
# Main Formulas 
#################
pr_base_dir = ('{pipe_pr_dir}', '{project}')

#################
# Asset Forumlas
#################
pr_as_dir = ('{pr_base_dir}', 'assets')
pr_as_type_dir = ('{pr_as_dir}', '{asset_type}')

# Valid 'asset_type' keys are: architecture, characters, foliage, massive, props, vehicles

# Formulas for finding asset specific documents: #see asset_formulas.txt file


#################
# Custom Formulas
#################
pr_data_dir = ('{pr_base_dir}', '.data')


#################
# Utility Formulas
#################

pr_pipe_form = ('{pipe_base_dir}', 'pipeline_formulas.txt')
pr_project_form = ('{pr_data_dir}', 'project_formulas.txt')
pr_asset_form = ('{pr_data_dir}', 'asset_formulas.txt')


#################
# DNA Forumlas
#################

pr_project_dna = ('{pr_data_dir}', 'project.dna')
pr_asset_dna = ('{pr_data_dir}', 'asset.dna')


# ---------------------------------------------------------------------------------------------
# Reference for the formulas to evaluate to initialize a project. Use these as a list in code.
# BASE_FORMULAS = ['pr_base_dir', 'pr_base_proj_dir',
# 'pr_as_dir', 'pr_data_dir', 'pr_epi_dir', 'pr_rsrc_dir', 'pr_config_dir',
# 'as_act_dir', 'as_char_dir', 'as_prop_dir', 'as_set_dir', 'pr_addon_dir']
#
# ---------------------------------------------------------------------------------------------